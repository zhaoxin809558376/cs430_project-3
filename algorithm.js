/**
 * quick sort chooses a random element in the array as the pivot
 * @param {*} A an random array[l..r]
 * @param {*} l left index of the array
 * @param {*} r right index of the array 
 */
 function quicksort_with_random(A, l, r) {
    if (l < r) {
        q = partition_random(A, l, r);
        quicksort_with_random(A, l, q - 1);
        quicksort_with_random(A, q + 1, r);
    }
    return A;
}

/**
 * quick sort where the pivot is chosen from the previous order statistics algorithms
 * @param {*} A an random array[l..r]
 * @param {*} l left index of the array
 * @param {*} r right index of the array 
 */
function quicksort_with_median(A, l, r) {
    if (l < r) {
        var n = r - l;
        var median = ithSmallest(A, l, r, Math.floor(n / 2), 5);
        q = partition_with_median(A, l, r, median);

        quicksort_with_median(A, l, q - 1);
        quicksort_with_median(A, q + 1, r);
    }
    return A;
}

/**
 * Return i'th smallest element in A[l..r] in worst case linear time.
 * @param {*} A a random array[l..r]
 * @param {*} l the most left index
 * @param {*} r the most right index
 * @param {*} ith i'th smallest element
 * @param {*} group_length divide the array by group_length size
 */
function ithSmallest(A, l, r, ith, group_length) {
    if (ith >= 0 && ith <= r - l) {
        var pivot = 0;
        var medians = [];
        var subArrays = [];

        for (let i = l; i < A.length; i += group_length) {
            subArrays.push(A.slice(i, i + group_length));
        }

        for (subArray of subArrays) {
            medians.push(subArray.sort(function(a, b){return a - b})[Math.floor(subArray.length / 2)]);
        }

        if (medians.length <= group_length) {
            pivot = medians.sort()[Math.floor(medians.length / 2)];
        } else {
            pivot = ithSmallest(medians, 0, medians.length - 1, Math.floor(medians.length / 2), group_length)
        }

        var pos = partition_with_median(A, l, r, pivot);

        if (pos - l == ith) {
            return A[pos];
        } else if (pos - l > ith) {
            return ithSmallest(A, l, pos - 1, ith, group_length);
        } else {
            return ithSmallest(A, pos, r, ith - pos + l, group_length);
        }
    }
}

/**
 * Partition with a random pivot
 * @param {*} A an random array[l..r]
 * @param {*} l left index of the array
 * @param {*} r right index of the array
 */
function partition_random(A, l, r) {
    let x = A[r];
    let i = l - 1;
    for (let j = l; j < r; j++) {
        if (A[j]<=x) {
            i++;
            swap(A, i, j)
        };
    }
    swap(A, i + 1, r);
    return i + 1;
}

/**
 * Passing the median as the pivot of partition
 * @param {*} A an random array[l..r]
 * @param {*} l left index of the array
 * @param {*} r right index of the array
 * @param {*} median median of the array
 */
function partition_with_median(A, l, r, median) {
    var index;
    for (index = 0; index < r; index++) {
        if (median == A[index])
            break;
    }
    swap(A, index, r);

    var x = A[r];
    var i = l - 1;
    for (let j = l; j < r; j++) {
        if (A[j] <= x) {
            i++;
            swap(A, i, j);
        } 
    }
    swap(A, i + 1, r);
    return i + 1;
}

/**
 * Swap index of index_a and index_b of the A
 * @param {*} index_a An index of the A
 * @param {*} index_b An index of the A
 */
 function swap(A, index_a, index_b) {
    if (A[index_a] != A[index_b]){
        let temp = A[index_a];
        A[index_a] = A[index_b];
        A[index_b] = temp;
    }
}